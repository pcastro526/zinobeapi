
const MongoClient = require('mongodb').MongoClient
const {ObjectId} = require('mongodb'); // or ObjectID 


      exports.conect_to_database= async function(){
        try {
            const uri = "mongodb+srv://ZiboneTest:ZiboneTest@cluster0.ifdxt.mongodb.net/Zinobe?retryWrites=true&w=majority";

          
            
            var mongoclient = new MongoClient(uri, { useNewUrlParser: true });
            
            // Open the connection to the server
            var db = await mongoclient.connect();
            
            return db;
        }catch(ex){
            console.log(ex);
            return null;
        }
    }

    exports.insert_to_collection= async function(collection, data) {

        try {
            //connection to database mongoDB
            
            var db = await this.conect_to_database();
            if(!db) return 
            var dbo = db.db(this.mongo_database);
            var res = await dbo.collection(collection).insertOne(data)
            db.close();
            return res;
        } catch (error) {
            console.log(error)
            return null;
        }
    }

    exports.get_data_by_collectionFind=  async function(collection, query){

        //connection to database mongoDB 
        var db = await this.conect_to_database();
        if(!db) return 
        var dbo = db.db(this.mongo_database);
        var data = await dbo.collection(collection).find(query).toArray();

        return data;
    }


     exports.get_data_by_collection= async function(collection) {

        //connection to database mongoDB 
        var db = await this.conect_to_database();
        if(!db) return 
        var dbo = db.db(this.mongo_database);
        var data = await dbo.collection(collection).find({}).toArray();

        return data;
    }

    exports.update_to_collection=async function(collection, filter, update) {
        
        try {
            //connection to database mongoDB
            var db = await this.conect_to_database();
            if(!db) return 
            var dbo = db.db(this.mongo_database);
            var res = await dbo.collection(collection).updateOne({_id : ObjectId(filter) }, update);
            
            db.close();
        } catch (error) {
            console.log(error)
        }
    }