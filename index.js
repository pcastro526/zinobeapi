const express = require('express')
const app = express()
const port = 3000
const oDataBase = require('./Services/MongoDB');
var bodyParser = require('body-parser')
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
// Add headers
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.get('/', (req, res) => {
  oDataBase.get_data_by_collection("Creditos").then(x => {
    res.send(x)
  })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

app.post('/add', function (req, res) {
  oDataBase.insert_to_collection("Creditos", req.body)
  res.send(req.body);
});

app.get('/FindCreditByUser/:cc', (req, res) => {

  const params = req.params;
  console.log(params.cc)
  oDataBase.get_data_by_collectionFind("Creditos", { cedula: params.cc }).then(x => {
    res.send(x)
  })

})


app.post('/upd', function (req, res) {
  var params= req.body
  console.log(params)
  oDataBase.update_to_collection("Creditos", params.filter,params.update)
   res.send(req.body);
});
